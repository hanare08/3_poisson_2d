// poisson_solver_2d.cpp : コンソール アプリケーションのエントリ ポイントを定義します。
//
// provides main entry point of poisson solver.

#include "stdafx.h"

#define METHOD MultiDirectionalFDM

#define N 101

#define EPS 1.0e-20

#define MAX_ITERATION 20000
#define MAX_VCYCLE 200
#define ITERATION_SMOOTHER 100
#define U_MAX 0.00032
#define RELAX 0.7

#define auto_const auto const

using std::vector;

enum method {
	Jacobi                    = 0,
	GaussSeidel               = 1,
	SuccessiveRelaxation      = 2,
	SuccessiveUnderRelaxation = 3,
	SuccessiveOverRelaxation  = 4,
	ConjugateGradient         = 5,
	MultiGrid                 = 6,
	MultiDirectionalFDM       = 7
};

static const char *method_name[] = {
"Jacobi",
"GS",
"SR",
"SUR",
"SOR",
"CG",
"MGrid",
"MDirFDM"
};

// returns the value of exact solution at point (x,y): u_exact(x,y).
inline double exact_solution(const double x, const double y) {
	return (x * x * (x - 1) * (x - 1) * (2 * x - 1) * y * y * (y - 1) * (y - 1) * (2 * y - 1));
}

// returns the value of right-hand term of Poisson Equation at point (x,y): f(x,y).
inline double calc_rhs(const double x, const double y) {
	return (2 * (2 * x - 1) * (10 * x * x - 10 * x + 1) * y * y *(y - 1) * (y - 1)* (2 * y - 1) \
		+ 2 * (2 * y - 1) * (10 * y* y - 10 * y + 1) * x * x *(x - 1) * (x - 1)* (2 * x - 1));
}

// initialization of variable u, rhs f, exact solution u_exact using given function pointers.
void init(const int nx, const int ny, std::vector<std::vector<double>>& u, std::vector<std::vector<double>>& f, std::vector<std::vector<double>>& u_exact, double(*calc_rhs)(double, double), double(*exact_solution)(double, double)) {
	// u is already initialized by filling 0.
	for (auto i = 0; i < nx; i++) {
		for (auto j = 0; j < ny; j++) {
			f[i][j] = calc_rhs((double)i / (double)(nx - 1), (double)j / (double)(ny - 1));
			u_exact[i][j] = exact_solution((double)i / (double)(nx - 1), (double)j / (double)(ny - 1));
		}
	}
	return;
}

// output of the result for each step
inline void output_u(const int nx, const int ny, const int step, const method mthd, const vector<vector<double>>& u, const bool is_exact)
{
	char buf[64];
	sprintf_s(buf, "results/%s%04d.dat", is_exact ? "exact" : method_name[mthd], step);
	std::ofstream ofs(buf);


	ofs << "# step = " << step << std::endl;
	for (auto i = 0; i < nx; i++) {
		for (auto j = 0; j < ny; j++) {
			ofs << (double)i / (double)(nx - 1.0) << "\t" << (double)j / (double)(ny - 1.0) << "\t" << u[i][j] << std::endl;
		}
	}
//	std::cout << std::endl;
//	ofs << std::endl;
//	ofs.close();

	return;
}


// overload for vector version of u.
inline void output_u(const int nx, const int ny, const int step, const method mthd, const vector<double>& u, const bool is_exact)
{
	char buf[64];
	const double dx = 1.0 / (double)(nx - 1.0), dy = 1.0 / (double)(ny - 1.0);
	sprintf_s(buf, "results/%s%04d.dat", is_exact ? "exact" : method_name[mthd], step);
	std::ofstream ofs(buf);

	ofs << "# step = " << step << std::endl;
	for (auto i = 1; i < nx-1; i++) {
		for (auto j = 1; j < ny-1; j++) {
			ofs << (double)i * dx << "\t" << (double)j / (double)(nx - 1.0) << "\t" << u[(j-1) * (nx-2) + (i-1)] << std::endl;
		}
	}
//	std::cout << std::endl;
//	ofs << std::endl;
//	ofs.close();

	return;
}


// initializes the coefficient matrix for Conjugate Gradient Method
vector<vector<double>> init_coefmat_cg(const int nx, const int ny, bool multidir) {
	if (nx < 2 || ny < 2) 
		return vector<vector<double>>{ {0.0} };
	// Most part of coef_mat is filled by 0 here.
	vector<vector<double>> coef_mat((nx - 2)*(ny - 2), vector<double>((nx - 2)*(ny - 2), 0.0));
	auto_const dxdx_inv = (double)((nx - 1) * (nx - 1)), dydy_inv = (double)((ny - 1) * (ny - 1));

	//Normal FDM
	if (!multidir) {
		for (auto i = 0; i < (nx - 2) * (ny - 2); i++) {
			coef_mat[i][i] = 2.0 * dxdx_inv + 2.0 * dydy_inv;
			if (i % (nx - 2) != 0) {
				coef_mat[i][i - 1] = -dxdx_inv;
			}
			if (i % (nx - 2) != nx - 3) {
				coef_mat[i][i + 1] = -dxdx_inv;
			}
			if (i >= nx - 2) {
				coef_mat[i][i - (nx - 2)] = -dydy_inv;
			}
			if (i < (nx - 2) * (ny - 3)) {
				coef_mat[i][i + (nx - 2)] = -dydy_inv;
			}
		}
	}

	//Multi Directional FDM
	else {
		auto_const dhdh_inv = 1.0 / (1.0 / (double)((nx - 1)*(nx - 1)) + (1.0/(double)((ny - 1)*(ny - 1))));
		for (auto i = 0; i < (nx - 2) * (ny - 2); i++) {
			// diagonal block
			coef_mat[i][i] = dxdx_inv * 4.0 / 3.0 + dydy_inv * 4.0 / 3.0 + dhdh_inv * 4.0 / 3.0;
			if (i % (nx - 2) != 0) {
				coef_mat[i][i - 1] = -dxdx_inv * 2.0 / 3.0;
			}
			if (i % (nx - 2) != nx - 3) {
				coef_mat[i][i + 1] = -dxdx_inv * 2.0 / 3.0;
			}

			// left block
			if (i >= nx - 2) {
				coef_mat[i][i - (nx - 2)] = -dydy_inv * 2.0 / 3.0;
				if (i % (nx - 2) != 0) {
					coef_mat[i][i - (nx - 2) - 1] = -dhdh_inv / 3.0;
				}
				if (i % (nx - 2) != nx - 3) {
					coef_mat[i][i - (nx - 2) + 1] = -dhdh_inv / 3.0;
				}
			}

			// right block
			if (i < (nx - 2) * (ny - 3)) {
				coef_mat[i][i + (nx - 2)] = -dydy_inv * 2.0 / 3.0;
				if (i % (nx - 2) != 0) {
					coef_mat[i][i + (nx - 2) - 1] = -dhdh_inv / 3.0;
				}
				if (i % (nx - 2) != nx - 3) {
					coef_mat[i][i + (nx - 2) + 1] = -dhdh_inv / 3.0;
				}
			}
		}
	}
	return(coef_mat);
}

// convert (nx x ny)-matrix into ((nx-2)x(ny-2))-vector, trimming both ends of matrix (thanks to Dirichlet condition.)
inline vector<double> mat2vec_trim(const int nx, const int ny, const vector<vector<double>>& mat) {
	if (nx < 2 || ny < 2) return vector<double>{0.0};
	vector<double> vec((nx - 2)*(ny - 2));
	for (auto i = 0; i < ny - 2; i++) {
		for (auto j = 0; j < nx - 2; j++) {
			vec[(nx - 2) * i + j] = mat[j + 1][i + 1];
		}
	}
	return vec;
}

// Multiplies (m x n) matrix A and n-vector b, then
// the result is overwritten on m-vector c.
template <typename T> inline void mul_mat_vec(vector<T>& c, const vector<vector<T>>& a, const vector<T>& b) {
	for (auto i = 0; i < a.size(); i++) {
		c[i] = 0.0;
		for (auto j = 0; j < a[i].size(); j++) {
			c[i] += a[i][j] * b[j];
		}
	}
	return;
}

// Applies operator whose type is (T,T) -> T to each elements of a, b.
// lambda's speed...?
template <typename T> inline void operation_2vec(const int n, vector<T>& result, const vector<T>& op1, const vector<T>& op2, T (*ope)(T, T)) {
	for (auto i = 0; i < n; i++) {
		result[i] = ope(op1[i], op2[i]);
	}
	return;
}

// Calculates residual vector r = f - Au
inline void calc_residual_vec(const int n, vector <double>& residual_vec, const vector<double>& f_vec, const vector<double>& u_vec, const vector<vector<double>>& coef_mat) {
	vector <double> au(n);
	mul_mat_vec<double>(au, coef_mat, u_vec);
	operation_2vec<double> (n, residual_vec, f_vec, au, [](double a, double b) {return a - b; });
	return;
}

// Initializes update-vector p by residual vector r (for now, and possibility of change remains here).
void init_p(const int n, vector<double>& p, vector<double>& residual) {
	for (auto i = 0; i < n; i++) {
		p[i] = residual[i];
	}
	return;
}

// Returns Improduct of vector a, b defined as (a, b)
inline double improduct(const int n, const vector<double>& a, const vector<double>& b) {
	double sum = 0.0;
	for (auto i = 0; i < n; i++) {
		sum += a[i] * b[i];
	}
	return sum;
}

// Returns A-Improduct of vector a, b defined as (a, Ab) .. will not be used.
inline double improduct_A(const int n, const vector<double>& a, const vector<double>& b, const vector<vector<double>>& coef_mat) {
	double sum = 0.0;
	for (auto i = 0; i < n; i++) {
		for (auto j	= 0; j < n; j++) {
			sum += a[i] * coef_mat[i][j] * b[j];
		}
	}
	return sum;
}

// Scalar multiplication. result vector will be overwritten.
// Variables: result and vec must have the same size.
inline void mul_scalar_vec(vector<double>& result, const double c, const vector<double>& vec) {
	for (auto i = 0; i < vec.size(); i++) {
		result[i] = c * vec[i];
	}
	return;
}

// Calculates coefficient alpha used in CG method.
// There may be some patterns to calc this?
//inline double calc_alpha(const int n, const vector<double>& p, const vector<double>& residual_vec, const vector<vector<double>>& coef_mat) {
//	return(improduct(n, residual_vec, p) / improduct_A(n, p, p, coef_mat));
//}

// Calculates coefficient beta used in CG method.
// There may be some patterns to calc this?
//inline double calc_beta(const int n, const vector<double>& p, const vector<double>& residual_vec, const vector<vector<double>>& coef_mat) {
//	return(- improduct_A(n, residual_vec, p, coef_mat) / improduct_A(n, p, p, coef_mat));
//}


// Update vector u by adding alpha * p.
// Residual in the same meaning as one in Jacobi can be captured here?
inline void update_u(const int n, vector<double>& u, const double alpha, vector<double>& p) {
	vector<double> ap(n);
	mul_scalar_vec(ap, alpha, p);
	operation_2vec<double>(n, u, u, ap, [](double a, double b) {return a + b; });
	return;
}

// Update vector u by adding alpha * p.
inline void update_p(const int n, vector<double>& p, const vector<double>& residual_vec, const double beta) {
	vector<double> bp(n);
	mul_scalar_vec(bp, beta, p);
	operation_2vec<double>(n, p, residual_vec, bp, [](double a, double b) {return a + b; });
	return;
}

// Returns error, which is difference between u and exact_solution.
inline double calc_error(const int nx, const int ny, const vector<double>& u, const vector<vector<double>>& u_exact) {
	double err = 0.0;
	for (auto i = 1; i < nx-1; i++) {
		for (auto j = 1; j < ny - 1; j++) {
			err += (u[(nx - 2) * (j - 1) + (i - 1)] - u_exact[i][j]) * (u[(nx - 2) * (j - 1) + (i - 1)] - u_exact[i][j]);			
		}
	}
	err = sqrt(err / (double)((nx - 2)*(ny - 2))) / U_MAX;
	return err;
}

// Returns residual, which is difference between new u and old one. This can be given by alpha and p in CG method.
inline double calc_residual_cg(const int n, const vector<double>& p, const double alpha) {
	return (abs(alpha) * sqrt(improduct(n, p, p) / (double)n) / U_MAX);
}

// Opens efs with method name and outputs error header.
inline void output_error_init(std::ofstream& efs, const int nx, const int ny, const method mthd, const double relaxation=1.0) {
	char buf[64];
	if (mthd == SuccessiveOverRelaxation || mthd == SuccessiveUnderRelaxation) {
		sprintf_s(buf, "error/%s%.1f.dat", method_name[mthd], relaxation);
	}
	else {
		sprintf_s(buf, "error/%s.dat", method_name[mthd]);
	}
	efs.open(buf);
	efs << "# (Spatial Resolution:) NX = " << nx << ", NY = " << ny << std::endl
		<< "# loop\tresidual\terror" << std::endl;
	
	return;
}

// Outputs error and residual to specified ofstream: efs.
inline void output_error(std::ofstream& efs, const int loop, const double error, const double residual) {
	efs << loop << "\t" << residual << "\t" << error << std::endl;
	return;
}

// Jacobi method.
// This solver also outputs residual and error at every 100 step and final step.
// Most part of this function is reused in solver_GS, 
// This is because performance I think precedes code-cleanness. (switch/if-else using mthd should not be included iteration loop.)
void solver_Jacobi(const int nx, const int ny, vector<vector<double>>& u, const vector<vector<double>>& f, const vector<vector<double>>& u_exact)
{
	const method mthd = Jacobi;
	auto_const dx = 1.0 / (double)(nx - 1), dy = 1.0 / (double)(ny - 1);
	const double dxdx_inv = (nx - 1) * (nx - 1), dydy_inv = (ny - 1)*(ny - 1);
	vector<vector<double>> u_next(u);
	double residual,error;
	auto loop=0, step=0;

	std::ofstream efs;

	//initialize efs and output error header
	output_error_init(efs, nx,ny,mthd);

	for (; loop < MAX_ITERATION; loop++) {
		// boundary condition is taken into consideration, when range of i,j is decided.
		residual = 0;
		error = 0;
		for (auto i = 1; i < nx - 1; i++) {
			for (auto j = 1; j < ny - 1; j++) {
				u_next[i][j] = ((u[i - 1][j] + u[i + 1][j]) * dxdx_inv + (u[i][j - 1] + u[i][j + 1]) *dydy_inv - f[i][j]) / (2.0 * dxdx_inv + 2.0 * dydy_inv);
				residual += (u_next[i][j] - u[i][j]) * (u_next[i][j] - u[i][j]);
				error += (u[i][j] - u_exact[i][j]) * (u[i][j] - u_exact[i][j]);
			}
		}
		error = sqrt((error / (double)((nx - 2) * (ny - 2)))) / U_MAX;
		residual = sqrt((residual / (double)((nx - 2) * (ny - 2)))) / U_MAX;
	
		if (!(loop % 100)) {
			output_error(efs, loop, error, residual);
			if (step <= 100) {
				output_u(nx, ny, step++, mthd, u, false);
			}
			std::cout << "loop=\t" << loop << "\tresidual =\t" << residual << "\terror =\t" << error << std::endl;
		}

		// test : residual is small enough?
		if (residual < EPS) {
			break;
		}
		u.assign(u_next.begin(), u_next.end());
	}
	output_error(efs, loop, error, residual);
	efs.close();
	std::cout << "loop=\t" << loop << "\tresidual =\t" << residual << "\terror =\t" << error << std::endl;
	std::cout << "Method: " << method_name[mthd] << " finished when iteration=" << loop << std::endl;

	return;
};

// Solver using Gauss Seidel Method
// This solver also outputs residual and error at every 100 step and final step.
void solver_GS(const int nx, const int ny, vector<vector<double>>& u, const vector<vector<double>>& f, const vector<vector<double>>& u_exact) 
{
	const method mthd = GaussSeidel;
	auto_const dx = 1.0 / (double)(nx - 1), dy = 1.0 / (double)(ny - 1);
	const double dxdx_inv = (nx - 1) * (nx - 1), dydy_inv = (ny - 1)*(ny - 1);
	double residual, error, uij_old;
	auto loop=0, step=0;

	std::ofstream efs;

	//initialize efs and output error header
	output_error_init(efs, nx, ny, mthd);

	for (; loop < MAX_ITERATION; loop++) {
		// boundary condition is taken into consideration, when range of i,j is decided.
		residual = 0;
		error = 0;
		for (auto i = 1; i < nx - 1; i++) {
			for (auto j = 1; j < ny - 1; j++) {
				uij_old = u[i][j];
				u[i][j] = ((u[i - 1][j] + u[i + 1][j]) *dxdx_inv + (u[i][j - 1] + u[i][j + 1]) * dydy_inv - f[i][j]) / (2 * dxdx_inv + 2 * dydy_inv);
				residual += (u[i][j] - uij_old) * (u[i][j] - uij_old);
				error += (uij_old - u_exact[i][j]) * (uij_old - u_exact[i][j]);
			}
		}
		error = sqrt((error / (double)((nx - 2) * (ny - 2)))) / U_MAX;
		residual = sqrt((residual / (double)((nx - 2) * (ny - 2)))) / U_MAX;
	
		if (!(loop % 100)) {
			output_error(efs, loop, error, residual);
			if (step <= 100) {
				output_u(nx, ny, step++, mthd, u, false);
			}
			std::cout << "loop=\t" << loop << "\tresidual =\t" << residual << "\terror =\t" << error << std::endl;
		}

		// test : residual is small enough?
		if (residual < EPS) {
			break;
		}
	}
	output_error(efs, loop, error, residual);
	efs.close();
	std::cout << "loop=\t" << loop << "\tresidual =\t" << residual << "\terror =\t" << error << std::endl;
	std::cout << "Method: " << method_name[mthd] << " finished when iteration=" << loop << std::endl;

	return;
};

// Successive Over/Under Relaxation method
// This solver also outputs residual and error at every 100 step and final step.
void solver_SR(const int nx, const int ny, vector<vector<double>>& u, const vector<vector<double>>& f, const vector<vector<double>>& u_exact, const double relaxation_factor)
{
	const method mthd = relaxation_factor > 1 ? SuccessiveOverRelaxation  :SuccessiveUnderRelaxation;
	auto_const dx = 1.0 / (double)(nx - 1), dy = 1.0 / (double)(ny - 1);
	const double dxdx_inv = (nx - 1) * (nx - 1), dydy_inv = (ny - 1)*(ny - 1);
	double residual, error, uij_old, uij_gs;
	auto loop=0, step=0;

	std::ofstream efs;

	//initialize efs and output error header
	output_error_init(efs, nx, ny, mthd, relaxation_factor);

	for (; loop < MAX_ITERATION; loop++) {
		// boundary condition is taken into consideration, when range of i,j is decided.
		residual = 0;
		error = 0;
		for (auto i = 1; i < nx - 1; i++) {
			for (auto j = 1; j < ny - 1; j++) {
				uij_old = u[i][j];
				uij_gs = ((u[i - 1][j] + u[i + 1][j]) *dxdx_inv + (u[i][j - 1] + u[i][j + 1]) * dydy_inv - f[i][j]) / (2 * dxdx_inv + 2 * dydy_inv);
				u[i][j] = (1.0 - relaxation_factor) * uij_old + relaxation_factor * uij_gs;
				residual += (u[i][j] - uij_old) * (u[i][j] - uij_old);
				error += (uij_old - u_exact[i][j]) * (uij_old - u_exact[i][j]);
			}
		}
		error = sqrt((error / (double)((nx - 2) * (ny - 2)))) / U_MAX;
		residual = sqrt((residual / (double)((nx - 2) * (ny - 2)))) / U_MAX;
	
		if (!(loop % 100)) {
			output_error(efs, loop, error, residual);
			if (step <= 100) {
				output_u(nx, ny, step++, mthd, u, false);
			}
			std::cout << "loop=\t" << loop << "\tresidual =\t" << residual << "\terror =\t" << error << std::endl;
		}

		// test : residual is small enough?
		if (residual < EPS) {
			break;
		}
	}
	output_error(efs, loop, error, residual);
	efs.close();
	std::cout << "loop=\t" << loop << "\tresidual =\t" << residual << "\terror =\t" << error << std::endl;
	std::cout << "Method: " << method_name[mthd] << " finished when iteration=" << loop << std::endl;

	return;
};

// Solver using Conjugate Gradient method.
void solver_CG(const int nx, const int ny, vector<vector<double>>& u_mat, const vector<vector<double>>& f_mat, const vector<vector<double>>& u_exact, bool multidir) {
	const method mthd = (multidir ? MultiDirectionalFDM : ConjugateGradient);
	const int n = (nx - 2)* (ny - 2);


	vector<double> residual_vec(n);
	vector<double> p(n);
	double alpha, beta, residual=0, error =0;
	auto loop = 0, step = 0;

	// temporary variables
	vector<double> ap(n);
	double prod_p_ap;

	std::ofstream efs;

	// initialization
	vector<double> f_vec = mat2vec_trim(nx, ny, f_mat);
	mul_scalar_vec(f_vec, -1.0, f_vec);
	vector<double> u_vec = mat2vec_trim(nx, ny, u_mat);
	vector<vector<double>> coef_mat = init_coefmat_cg(nx, ny, multidir);

	calc_residual_vec(n, residual_vec, f_vec, u_vec, coef_mat);
	init_p(n, p, residual_vec);
	output_error_init(efs, nx, ny,mthd);
	output_u(nx, ny, step++, mthd, u_vec, false);


	for (; loop < n; loop++) {
		//calc_alpha .. reusing improduct and vector ap.
		mul_mat_vec(ap, coef_mat, p);
		prod_p_ap = improduct(n, p, ap);
		alpha = improduct(n, residual_vec, p) / prod_p_ap;
		//		alpha = calc_alpha(n, p, residual_vec, coef_mat);
		residual = calc_residual_cg(n, p, alpha);
		error = calc_error(nx, ny, u_vec, u_exact);
		output_error(efs, loop, error, residual);
		std::cout << "loop\t" << loop << ":\tresidual =\t" << residual << "\terror =\t" << error << std::endl;


		update_u(n, u_vec, alpha, p);
		calc_residual_vec(n, residual_vec, f_vec, u_vec, coef_mat);

		//calc_beta
		beta = - improduct(n, residual_vec, ap) / prod_p_ap;
		//		beta = calc_beta(n, p, residual_vec, coef_mat);
		
		update_p(n, p, residual_vec, beta);

		if (step < 100) {
			output_u(nx, ny, step++, mthd, u_vec, false);
		}
		if (residual < EPS) {
			break;
		}
	}

	efs.close();
	std::cout << "Method: " << method_name[mthd] << " finished when iteration=" << loop << std::endl;


	return;
}

// Solver using 2-level Multi Grid method. Each level uses Gauss Seidel method as the smoothing operator.
void solver_MultiGrid(const int nx, const int ny, vector<vector<double>>& u, const vector<vector<double>>& f, const vector<vector<double>>& u_exact) 
{
	const method mthd = MultiGrid;
	const double dx = 1.0 / (double)(nx - 1), dy = 1.0 / (double)(ny - 1);
	const double dxdx_inv = (nx - 1) * (nx - 1), dydy_inv = (ny - 1)*(ny - 1);
	double residual, error, tmp_old;
	auto loop=0, step=0, loop_idx=0;

	//for coarse grid
	const int nxc = (nx + 2) / 2, nyc = (ny + 2) / 2;
	const double dxcdxc_inv = dxdx_inv / 4.0, dycdyc_inv = dydy_inv / 4.0;
	vector<vector<double>> residual_mat(nxc, vector<double>(nyc, 0.0));
	vector<vector<double>> du(nxc, vector<double>(nyc, 0.0));

	std::ofstream efs;

	//initialize efs and output error header
	output_error_init(efs, nx, ny, mthd);

	for (loop=0; loop < MAX_VCYCLE; loop++) {

		// 1.fine level: Gauss Seidel method
		// calc u s.t. Au=f
		for (auto loop_smoother = 0; loop_smoother < ITERATION_SMOOTHER; loop_smoother++) {
			residual = 0.0;
			error = 0.0;
			for (auto i = 1; i < nx - 1; i++) {
				for (auto j = 1; j < ny - 1; j++) {
					tmp_old = u[i][j];
					u[i][j] = ((u[i - 1][j] + u[i + 1][j]) * dxdx_inv + (u[i][j - 1] + u[i][j + 1]) * dydy_inv - f[i][j]) / (2 * dxdx_inv + 2 * dydy_inv);
					residual += (u[i][j] - tmp_old) * (u[i][j] - tmp_old);
					error += (tmp_old - u_exact[i][j]) * (tmp_old - u_exact[i][j]);
				}
			}
			error = sqrt((error / (double)((nx - 2) * (ny - 2)))) / U_MAX;
			residual = sqrt((residual / (double)((nx - 2) * (ny - 2)))) / U_MAX;

			if (!(loop_idx % 10)) {
				output_error(efs, loop_idx, error, residual);
				if (step <= 100) {
					output_u(nx, ny, step++, mthd, u, false);
				}
				std::cout << "loop=\t" << loop_idx << "\tresidual =\t" << residual << "\terror =\t" << error << std::endl;
			}

			//　break from inner loop
			if (residual < EPS) {
				std::cout << "Break at fine level loop= " << loop_smoother << std::endl;
				break;
			}
			loop_idx++;
		}
		std::cout << "Iteration in Fine   Level Smoother (Gauss Seidel) finished with residual = " << residual << std::endl;

		//　break from outer loop
		if (residual < EPS) {
			break;
		}


		// 2.calc residual matrix r (used as r.h.s. instead of f )
		// 3.restriction operator: fine r to coarse r
		// assign coarse grid to 
		//		i = 2,4,6,8 ... 2*(nx-2)/2
		// 		j = 2,4,6,8 ... 2*(ny-2)/2
		// in fine grid. (points nearby boundary are excluded here.)
		for (auto i = 1; i < nxc - 1; i++) {
			for (auto j = 1; j < nyc - 1; j++) {
				residual_mat[i][j] = f[2 * i][2 * j] \
					- (u[2 * i - 1][2 * j] - 2.0 * u[2 * i][2 * j] + u[2 * i + 1][2 * j]) * dxdx_inv \
					- (u[2 * i][2 * j - 1] - 2.0 * u[2 * i][2 * j] + u[2 * i][2 * j + 1]) * dydy_inv;
			}
		}


		//// 4.coarse level: Gauss Seidel method
		//// calc du s.t. A(du)=r
		
		du.assign(nxc, vector<double>(nyc, 0.0));
		for (auto loop_smoother = 0; loop_smoother < ITERATION_SMOOTHER; loop_smoother++) {
			residual = 0.0;
			for (auto i = 1; i < nxc - 1; i++) {
				for (auto j = 1; j < nyc - 1; j++) {
					tmp_old = du[i][j];
					du[i][j] = ((du[i - 1][j] + du[i + 1][j]) * dxcdxc_inv + (du[i][j - 1] + du[i][j + 1]) * dycdyc_inv - residual_mat[i][j]) / (2 * dxcdxc_inv + 2 * dycdyc_inv);
					residual += (du[i][j] - tmp_old) * (du[i][j] - tmp_old);
				}
			}
			residual = sqrt((residual / (double)((nxc - 2) * (nyc - 2)))) / U_MAX;
			if (residual < EPS) {
				std::cout << "Break at coarse level loop= " << loop_smoother << std::endl;
				break;
			}
		}
		std::cout << "Iteration in Coarse Level Smoother (Gauss Seidel) finished with residual = " << residual << std::endl;

		//// 5.prolongation operator: coarse du to fine du, and update u by u+du
		//// 6.linear interpolation for unupdated values
		for (auto i = 1; i < nx - 1; i++) {
			for (auto j = 1; j < ny - 1; j++) {
				if (i % 2 && j % 2) {
					u[i][j] += (du[(i - 1) / 2][(j - 1) / 2] + du[(i - 1) / 2][(j + 1) / 2] + du[(i + 1) / 2][(j - 1) / 2] + du[(i + 1) / 2][(j + 1) / 2]) / 4.0;
				}
				else if (!(i % 2) && j % 2) {
					u[i][j] += (du[i / 2][(j - 1) / 2] + du[i / 2][(j + 1) / 2]) / 2.0;
				}
				else if (i % 2 && !(j % 2)) {
					u[i][j] += (du[(i - 1) / 2][j / 2] + du[(i + 1) / 2][j / 2]) / 2.0;
				}
				else if (!(i % 2) && !(j % 2)) {
					u[i][j] += du[i / 2][j / 2];
				}
			}
		}

	}

	efs.close();
	std::cout << "loop=\t" << loop_idx << "\tresidual =\t" << residual << "\terror =\t" << error << std::endl;
	std::cout << "Method: " << method_name[mthd] << " finished when iteration=" << loop_idx << std::endl;

	return;
};

// Poisson Equation Solver, whose solution method is specified by argument: mthd.
void solver_select(method mthd, const int nx, const int ny, vector<vector<double>>& u, const vector<vector<double>>& f, const vector<vector<double>>& u_exact, const double relaxation) {
	//int(*solver_ls[])(const int, const int, vector<vector<double>>&, const vector<vector<double>>&, const vector<vector<double>>&) = { solver_Jacobi, solver_GS, solver_SR, solver_CG};
	//	//		, solver_MultiGrid, solver_MultiDirectionalFDM
	//solver_ls[mthd](nx, ny, u, f, u_exact);
	switch (mthd) {
	case Jacobi:
		solver_Jacobi(nx, ny, u, f, u_exact);
		break;
	case GaussSeidel:
		solver_GS(nx, ny, u, f, u_exact);
		break;
	case SuccessiveRelaxation:
		solver_SR(nx, ny, u, f, u_exact, relaxation);
		break;
	case ConjugateGradient:
		solver_CG(nx, ny, u, f, u_exact, false);
		break;
	case MultiGrid:
		solver_MultiGrid(nx, ny, u, f, u_exact);
		break;
	case MultiDirectionalFDM:
		solver_CG(nx, ny, u, f, u_exact, true);
		break;
	default:
		break;
	}
};

// test of initialization in coefficient matrix A in cg solver.
bool test_init_coefmat_cg() {
	auto const n = 3, m = 3;
	vector<vector<double>> coef_mat = init_coefmat_cg(n, m, false);
	//for (auto i = 0; i < (n - 2)*(m - 2); i++) {
	//	for (auto j = 0; j < (n - 2)*(m - 2); j++) {
	//		std::cout << "\t" << coef_mat[i][j];
	//	}
	//	std::cout << std::endl;
	//}
	return true;
}

// test of initialization in coefficient matrix A in cg solver, using multi directional option.
bool test_init_coefmat_cg_multidir() {
	auto const n = 7, m = 7;
	vector<vector<double>> coef_mat = init_coefmat_cg(n, m, true);
	for (auto i = 0; i < (n - 2)*(m - 2); i++) {
		for (auto j = 0; j < (n - 2)*(m - 2); j++) {
			std::cout << "\t" << coef_mat[i][j];
		}
		std::cout << std::endl;
	}
	return true;
}

// test of convertion from matrix into vector with trimming.
bool test_mat2vec() {
	auto const nx = 5, ny = 4;
	vector<vector<double>> f{ {1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16},{17,18,19,20} };
	vector<double> f_rhs_exact{ 6,10,14,7,11,15 };
	vector<double> f_rhs = mat2vec_trim(nx, ny, f);
	//for (auto i = 0; i < (nx - 2)*(ny - 2); i++) {
	//	std::cout << f_rhs[i] << std::endl;
	//}
	return (f_rhs == f_rhs_exact);
}

// test of application of operator between 2 vector elements.
bool test_vector_op() {
	vector<double> r(4), a{ 1,2,3,4 }, b{ 5,6,7,8 };
	vector<vector<double>> exact{ { 6,8,10,12 }, { 5,12,21,32 },{-4,-4,-4,-4} };
	operation_2vec<double>(4, r, a, b, [](double x, double y) {return x + y; });
	if (r != exact[0]) 
		return false;
	operation_2vec<double>(4, r, a, b, [](double x, double y) {return x * y; });
	if (r != exact[1]) 
		return false;
	operation_2vec<double>(4, r, a, b, [](double x, double y) {return x - y; });
	if (r != exact[2]) 
		return false;
	return (true);
}

// test of application of operator between 2 vector elements.
bool test_mul_mat_vec() {
	auto const n = 3;
	vector<vector<double>> a{ { 1,2,3}, {2,5,-1}, {3,2,1} };
	vector<double> b{ 1,2,3 }, c(n, 0);
	vector<vector<double>> exact{ { 14, 9, 10} };
	mul_mat_vec(c, a, b);
	return (c == exact[0]);
}

// test of u = u + ap.
bool test_update_u() {
	auto const n = 3;
	vector<double> a{ 1,2,3 }, b{ 2,4,5 };
	double alpha = 0.8, error=0.0;
	vector<vector<double>> exact{ { 2.6, 5.2, 7.0} };
	update_u(n, a, alpha, b);
	for (auto i = 0; i < n; i++) {
		error += abs(a[i] - exact[0][i]);
	}
	return (error < EPS);
}

// test of p = r + bp.
bool test_update_p() {
	auto const n = 3;
	vector<double> a{ 1,2,3 }, r{ 2,4,5 };
	double beta = 0.8, error=0.0;
	vector<vector<double>> exact{ { 2.8, 5.6, 7.4} };
	update_p(n, a, r, beta);
	for (auto i = 0; i < n; i++) {
		error += abs(a[i] - exact[0][i]);
	}
	return (error < EPS);
}

// This can be rewrited using a class, like class unitTest(...) ?
bool tests() {
	int test_num=0;
	auto test_suc=0;

	vector<bool(*)(void)> test_ls{ \
		test_init_coefmat_cg, \
		test_mat2vec, \
		test_vector_op, \
		test_mul_mat_vec, \
		test_update_u, \
		test_update_p, \
		test_init_coefmat_cg_multidir };

	for (auto i = 0; i<test_ls.size(); i++){
		test_num++;
		if (test_ls[i]()) test_suc++;
	}
	

	std::cerr << test_suc << " of " << test_num << " tests succeeded." << std::endl;

	return (test_num==test_suc);
}

int main()
{
	if (!tests()) {
		return (EXIT_FAILURE);
	}

	auto_const nx = N, ny = N;
	const double relaxation = RELAX;

	if (nx < 3 || ny < 3) {
		std::cerr << "Both of nx and ny must be larger than 2." << std::endl;
		exit(EXIT_FAILURE);
	}

	// u is partially initialized here.
	vector<vector<double>> u(nx, vector<double>(ny, 0)), u_exact(nx, vector<double>(ny)), f(nx, vector<double>(ny));

	init(nx, ny, u, f, u_exact, calc_rhs, exact_solution);
	output_u(nx, ny, 0, METHOD, u_exact, true);
	solver_select(METHOD, nx, ny, u, f, u_exact, relaxation);

    return 0;
}

// todo:
// - Convert GS-method into a subroutine so that it can be used in multigrid.
// - Refactoring multigrid.
