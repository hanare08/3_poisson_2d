# README


## Short Summary

* **Learning C++ (3)**: 2D Poisson Equation Solver using various iterative methods.
* `poison_solver_2d` includes following methods implemented.
    - Jacobi method
    - Gauss-Seidel method
    - Successive Over/Under Relaxation method
    - Conjugate Gradient method
    - Multi Grid method using Gauss Seidel as a smoother
    - Multi Directional Finite Differential Method using Conjugate Gradient.
* Common powerpoint report on this assignment is on the S***Y.

## Execution (for furture)

* This project was created using Visual Studio Enterprise 2015.